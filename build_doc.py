import os
import sys
import shutil
import webbrowser
import subprocess
from pathlib import Path
try:
    import sphinx
except ImportError:
    raise NameError("Cannot find sphinx in selected interpreter.")

# Build each package .rst file
for package in Path(os.path.abspath('packages')).iterdir():
    if not package.is_dir():
        continue
    cmd = 'sphinx-apidoc -o docs/source/packages {}/python/ -f -T -E -t source/_templates'.format(package)
    subprocess.call(cmd)
    print('{} documentation build'.format(package))

# Build the sphinx pipeline documentation
source_path = os.path.abspath('docs/source').replace(os.sep, '/')
build_path = os.path.abspath('docs/build').replace(os.sep, '/')
shutil.rmtree(build_path)  # Remove existing build folder
sys.argv.extend(['-b', 'html', source_path, build_path])
version = sphinx.version_info
if (version[0] >= 1 and version[1] >= 7) or version[0] >= 2:
    from sphinx.cmd import build
    build.main(sys.argv[1:])
else:
    from sphinx import cmdline
    cmdline.main(sys.argv)
# Open the browser with the build doc
webbrowser.open('file://' + os.path.join(build_path, 'index.html'))
